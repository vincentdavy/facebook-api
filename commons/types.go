package commons

//FacebookPost represents Facebook post for API side
type FacebookPost struct {
	Text string `json:"text"`
	URL  string `json:"url"`
}

//FacebookGraph Facebook API graph data
type FacebookGraph struct {
	Data []FacebookPostItem `json:"data"`
}

//FacebookPostItem graph data from feed
type FacebookPostItem struct {
	Message string `json:"message,omitempty"`
	Story   string `json:"story,omitempty"`
	Link    string `json:"permalink_url"`
}
