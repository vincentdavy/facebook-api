package config

import (
	"errors"
	"fmt"
	"net/url"
	"os"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var configLogger = log.WithField("logger", "config")

//Config struct for config data
type Config struct {
	MainConfig     MainConfig     `yaml:"main"`
	FacebookConfig FacebookConfig `yaml:"facebook"`
}

//FacebookConfig struct for wordpress config
type FacebookConfig struct {
	URL          string   `yaml:"url"`
	DefaultLimit int      `yaml:"defaultLimit"`
	Fields       []string `yaml:"fields"`
}

//MainConfig struct for server config
type MainConfig struct {
	ServerPort      int    `yaml:"server-port"`
	MySQLConnection string `yaml:"mysqlURL"`
}

//NewConfig load and parse the config
func NewConfig(configFilePath string) (*Config, error) {
	configLogger.WithField("ConfigFile", configFilePath).Debug("Loading conf file")
	configFile, err := os.Open(configFilePath)
	if err != nil {
		return nil, err
	}

	config := &Config{}
	err = yaml.NewDecoder(configFile).Decode(&config)
	if err != nil {
		return nil, err
	}

	if err = config.MainConfig.checkMainConfig(); err != nil {
		return nil, err
	}
	if err = config.FacebookConfig.checkFacebookConfig(); err != nil {
		return nil, err
	}

	configLogger.Debugf("Config values : %+v", config)
	configLogger.Infof("Config loaded")
	return config, nil
}

func (mainConfig MainConfig) checkMainConfig() error {
	if mainConfig.ServerPort <= 0 {
		return errors.New("port must be over 0")
	}

	if len(mainConfig.MySQLConnection) == 0 {
		return errors.New("mysql connection URL must be provided")
	}
	return nil
}

func (facebookConfig FacebookConfig) checkFacebookConfig() error {
	if _, err := url.ParseRequestURI(facebookConfig.URL); err != nil {
		return fmt.Errorf("invalid Facebook URL : %s", facebookConfig.URL)
	}
	if len(facebookConfig.Fields) == 0 {
		return errors.New("fields are missing")
	}
	if facebookConfig.DefaultLimit == 0 {
		return errors.New("default limit is missing")
	}

	return nil
}
