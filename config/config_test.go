package config

import (
	log "github.com/sirupsen/logrus"
	"testing"
)

var configTestLogger = log.WithField("logger", "configTest")

func init() {
	log.SetLevel(log.DebugLevel)
}

func TestNewConfig_NoFile(t *testing.T) {
	config, err := NewConfig("")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_WrongFile(t *testing.T) {
	config, err := NewConfig("config.go")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_GoodFile(t *testing.T) {
	config, err := NewConfig("../config-file/config.yaml")
	if config == nil || err != nil {
		t.Errorf("Config nil or error not nil : %v, %v", config, err)
	}
}

func TestNewConfig_ServerNoPort(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-serverNoPort.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_NoMysql(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-noMysql.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_FacebookNoURL(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-facebookNoURL.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_FacebookNoDefaultLimit(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-facebookNoDefaultLimit.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_FacebookNoFields(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-facebookNoFields.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_NoFacebook(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-noFacebook.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}

func TestNewConfig_NoServer(t *testing.T) {
	config, err := NewConfig("../config-file/test/config-noServer.yaml")
	if config != nil || err == nil {
		t.Errorf("Config not nil or error nil : %v, %v", config, err)
	}
	configTestLogger.WithField("Error", err).Info("Error returned")
}
