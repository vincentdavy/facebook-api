package mysql

import (
	"database/sql"
	"facebook-api/config"
	_ "github.com/go-sql-driver/mysql" //mysql driver
	log "github.com/sirupsen/logrus"
)

const (
	mysqlDriverName = "mysql"
	sqlQuery        = `SELECT option_value FROM wp_options WHERE option_name = 'cff_access_token'`
)

var (
	db   *sql.DB
	stmt *sql.Stmt
)

var mysqlLogger = log.WithField("logger", "mysql")

//InitDBConn initialize the DB connection at startup
func InitDBConn(config *config.Config) error {
	var err error
	db, err = sql.Open(mysqlDriverName, config.MainConfig.MySQLConnection)
	if err != nil {
		return err
	}

	// testing connection
	if err = db.Ping(); err != nil {
		return err
	}

	// preparing query statement
	stmt, err = db.Prepare(sqlQuery)
	if err != nil {
		return err
	}

	mysqlLogger.Info("DB connection opened")
	return nil
}

//CloseDBConnection close DB connection when ending
func CloseDBConnection() {
	stmt.Close()
	db.Close()
	mysqlLogger.Info("DB connection closed")
}

//GetFacebookAPIToken return the token to access Facebook graph API
func GetFacebookAPIToken() string {
	mysqlLogger.Debug("Searching Facebook API token")

	var token string
	err := stmt.QueryRow().Scan(&token)
	if err != nil {
		mysqlLogger.WithError(err).Error("Error loading Facebook API token")
		return ""
	}

	mysqlLogger.WithField("Token", token).Debug("Facebook API token found")
	return token
}
