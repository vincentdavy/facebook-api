package process

import (
	"encoding/json"
	"facebook-api/commons"
	"facebook-api/config"
	"facebook-api/mysql"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

const (
	limitQueryStringParam  = "limit"
	tokenQueryStringParam  = "access_token"
	fieldsQueryStringParam = "fields"
)

var facebookPostLogger = log.WithField("logger", "facebookPost")

//GetFacebookFeed returns the Facebook feed
func GetFacebookFeed(limit int, config *config.Config) []commons.FacebookPost {
	facebookPostLogger.Debugln("Facebook feed request")

	if resp, err := http.Get(buildURL(limit, config)); err == nil && resp.StatusCode == http.StatusOK {
		defer resp.Body.Close()
		var facebookGraph commons.FacebookGraph
		if err = json.NewDecoder(resp.Body).Decode(&facebookGraph); err == nil {
			if len(facebookGraph.Data) > 0 {
				return processFacebookGraphData(facebookGraph.Data)
			}
		} else {
			facebookPostLogger.WithError(err).Error("Error unmarshalling Facebook graph data")
		}
	} else {
		facebookPostLogger.WithError(err).WithField("HTTPCode", resp.StatusCode).Error("Error with Facebook API")
	}
	return []commons.FacebookPost{}
}

func processFacebookGraphData(facebookGraph []commons.FacebookPostItem) []commons.FacebookPost {
	var facebookPosts []commons.FacebookPost
	for _, facebookGraphPost := range facebookGraph {
		post := commons.FacebookPost{
			URL: facebookGraphPost.Link,
		}
		if facebookGraphPost.Message != "" {
			post.Text = facebookGraphPost.Message
		} else {
			post.Text = facebookGraphPost.Story
		}
		facebookPosts = append(facebookPosts, post)
	}
	facebookPostLogger.Debugf("All facebookPosts loaded : %+v", facebookPosts)
	return facebookPosts
}

func buildURL(limit int, config *config.Config) string {
	url, err := url.Parse(config.FacebookConfig.URL)
	if err == nil {
		query := url.Query()
		query.Add(limitQueryStringParam, strconv.Itoa(limit))
		query.Add(fieldsQueryStringParam, strings.Join(config.FacebookConfig.Fields, ","))
		query.Add(tokenQueryStringParam, mysql.GetFacebookAPIToken())
		url.RawQuery = query.Encode()
		facebookPostLogger.WithField("URL", url.String()).Debug("Facebook API URL")
		return url.String()
	}

	facebookPostLogger.WithError(err).Error("Error parsing Facebook API URL")
	return ""
}
