package process

import (
	"facebook-api/commons"
	"testing"
)

func TestProcessFacebookGraphData(t *testing.T) {
	facebookGraph := []commons.FacebookPostItem{
		{Message: "test", Link: "link"},
		{Story: "test2", Link: "link2"},
	}
	feed := processFacebookGraphData(facebookGraph)

	if len(feed) != 2 {
		t.Error("Expected length array of 2")
	}
	if feed[0].Text != "test" || feed[0].URL != "link" {
		t.Error("Wrong conversion for item 1")
	}
	if feed[1].Text != "test2" || feed[1].URL != "link2" {
		t.Error("Wrong conversion for item 2")
	}
}
