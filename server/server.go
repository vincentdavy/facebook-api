package server

import (
	"encoding/json"
	"facebook-api/commons"
	"facebook-api/config"
	"facebook-api/process"
	"net/http"
	"net/url"
	"strconv"

	log "github.com/sirupsen/logrus"
)

const (
	apiPrefix                     = "/facebook-api/v1"
	limitParamName                = "limit"
	contentType, contentTypeValue = "Content-Type", "application/json; charset=utf-8"
)

var (
	serverLogger = log.WithField("logger", "server")
	headers      = map[string]string{
		contentType: contentTypeValue,
	}
)

//StartHTTPServer starts the HTTP port on specified port and binds all API URL
func StartHTTPServer(config *config.Config) error {
	serverLogger.WithField("Port", config.MainConfig.ServerPort).Info("Starting HTTP server")
	http.HandleFunc(apiPrefix+"/feed", handleFacebookFeed(config))
	return http.ListenAndServe(":"+strconv.Itoa(config.MainConfig.ServerPort), nil)
}

func handleFacebookFeed(config *config.Config) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		defer request.Body.Close()
		var foundFacebookPosts []commons.FacebookPost
		limit := paramsExtractor(request, config)
		foundFacebookPosts = process.GetFacebookFeed(limit, config)
		setHeaders(writer)
		if err := json.NewEncoder(writer).Encode(foundFacebookPosts); err != nil {
			serverLogger.WithError(err).Warn("Error with found Facebook posts serialization")
		}
	}
}

func setHeaders(writer http.ResponseWriter) {
	for header, value := range headers {
		writer.Header().Set(header, value)
	}
}

func paramsExtractor(request *http.Request, config *config.Config) int {
	query := request.URL.Query()
	return paramIntExtractor(query, limitParamName, config.FacebookConfig.DefaultLimit)
}

func paramIntExtractor(values url.Values, paramName string, defaultValue int) int {
	if paramStringValue := values.Get(paramName); len(paramStringValue) > 0 {
		if paramIntValue, err := strconv.Atoi(paramStringValue); err == nil {
			return paramIntValue
		}
		return defaultValue
	}
	return defaultValue
}
