package server

import (
	"net/url"
	"testing"
)

func TestParamIntExtractor_EmptyValue(t *testing.T) {
	paramValue := paramIntExtractor(url.Values{}, "test", 10)
	if paramValue != 10 {
		t.Error("Wrong value received")
	}
}

func TestParamIntExtractor_WrongValueName(t *testing.T) {
	values := url.Values{}
	values.Add("test2", "20")
	paramValue := paramIntExtractor(values, "test", 10)
	if paramValue != 10 {
		t.Error("Wrong value received")
	}
}

func TestParamIntExtractor_WrongString(t *testing.T) {
	values := url.Values{}
	values.Add("test2", "toto")
	paramValue := paramIntExtractor(values, "test", 10)
	if paramValue != 10 {
		t.Error("Wrong value received")
	}
}

func TestParamIntExtractor_GoodCase(t *testing.T) {
	values := url.Values{}
	values.Add("test", "20")
	paramValue := paramIntExtractor(values, "test", 10)
	if paramValue != 20 {
		t.Error("Wrong value received")
	}
}
